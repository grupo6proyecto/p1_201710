package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoubleLinkedList<T> implements ILista<T>{
	
/*-------------------------		Atributos	----------------------------------*/
	
	/**
	 * Primer nodo de la lista.
	 */
	private DoubleNode<T> cabeza;
	
	/**
	 * Ultimo nodo de la lista.
	 */
	private DoubleNode<T> cola;
	
	/**
	 * Tama�o de la lista.
	 */
	private int size;
	
	/**
	 * Iterador de lalista.
	 */
	private Iterator<T> iter;
	
	/**
	 * Posicion de referencia de la lista
	 */
	private int ref;
	

	/*-------------------------		Constructor	----------------------------------*/
	
	public DoubleLinkedList(){
		size = 0;
		cabeza = null;
		cola = null;
		ref = 0;
		
		iter = new Iterator<T>() {
			
			private DoubleNode<T> actual = cabeza;
			
			@Override
			public boolean hasNext(){
				if(actual==null)
					return false;
				else if(actual.next==null)
					return false;
				else
					return true;
			}
			
			@Override
			public T next(){
				if(hasNext()){
					actual = actual.next;
					return actual.item;
				}else
					throw new NoSuchElementException("No hay mas elementos.");
			}
		};
	}

	/*-------------------------		Metodos		----------------------------------*/

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return iter;
	}

	@Override
	public void add(T elem) {
		// TODO Auto-generated method stub
		DoubleNode<T> n = new DoubleNode<T>();
		n.item = elem;
		n.next = null;
		if(size == 0){
			n.prev = null;
			cabeza = n;
			cola = cabeza;
		}else{
			n.prev = cola;
			cola.next = n;
			cola = cola.next;
		}
		size++;
	}

	@Override
	public void addFirst(T e) {
		// TODO Auto-generated method stub
		DoubleNode<T> n = new DoubleNode<T>();
		n.item = e;
		n.prev = null;
		if(size==0){
			n.next = null;
			cabeza = n;
			cola = cabeza;
		}else{
			n.next = cabeza;
			cabeza.prev = n;
			cabeza = cabeza.prev;
		}
		size++;
	}
	
	@Override
	public boolean set(T e,int pos){
		try{
			DoubleNode<T> actual = cabeza;
			int i = 0;
			while(i<pos){
				i++;
				actual = actual.next;
			}
			actual.item = e;
			return true;
		}catch(Exception n){
			return false;
		}
	}

	@Override
	public T get(int pos)throws ArrayIndexOutOfBoundsException {
			if(!(pos < size) || pos<0)
				throw new ArrayIndexOutOfBoundsException("No existe dicha posici�n.");
			ref = pos;
			if(pos == size-1)
				return cola.item;
			else{
				DoubleNode<T> actual = cabeza;
				for(int i = 0; i<pos ; i++) {
					actual = actual.next;
				}
				return actual.item;
			}
			
	}

	@Override
	public T remove(int pos) {
		// TODO Auto-generated method stub
		if(pos == 0)
			return(removeFirst());
		if(pos == size-1)
			return(removeLast());
		
		if(size==0)
			throw new NoSuchElementException("No hay elementos en la lista");
		if(pos<0||pos>size){
			throw new ArrayIndexOutOfBoundsException("No existe dicha posici�n.");
		}
		else{
			DoubleNode<T> actual = cabeza;
			for (int i = 0; i < size; i++) {
				if(i == pos){
					actual.prev.next = actual.next;
					actual.next.prev = actual.prev;
					actual.next = null;
					actual.prev = null;
					size--;
					return actual.item;
				}
				else
					actual = actual.next;
			}
		}
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T removeFirst() {
		// TODO Auto-generated method stub
		if(size==0)
			throw new NoSuchElementException("No hay elementos en la lista");
		else if(size==1){
			DoubleNode<T> actual = cabeza;
			cabeza = null;
			cola = cabeza;
			size--;
			return actual.item;
		}
		else{
			DoubleNode<T> actual = cabeza;
			cabeza = cabeza.next;
			cabeza.prev = null;
			actual.next = null;
			actual.prev = null;
			size--;
			return actual.item;
		}
	}

	@Override
	public T removeLast() {
		// TODO Auto-generated method stub
		if(size==0)
			throw new NoSuchElementException("No hay elementos en la lista");
		else if(size==1){
			DoubleNode<T> actual = cabeza;
			cabeza = null;
			cola = cabeza;
			size--;
			return actual.item;
		}
		else{
			DoubleNode<T> actual = cola;
			cola = cola.prev;
			cola.next = null;
			actual.next = null;
			actual.prev = null;
			size--;
			return actual.item;
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}
	
	/**
	 * Clase NodoDoble.
	 * @author jm.contreras10
	 */
	private class DoubleNode<T>{
		private T item;
		private DoubleNode<T> next;
		private DoubleNode<T> prev;
	}
}
