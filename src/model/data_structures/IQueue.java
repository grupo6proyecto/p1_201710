package model.data_structures;

import java.security.NoSuchAlgorithmException;

public interface IQueue<T>{
	
	/**
	 * Agrega un item en la �ltima posici�n de la cola.
	 * @param item a agregar.
	 */
	public void enQueue(T item);
	
	/**
	 * Elimina el elemento en la primera posici�n de la cola.
	 * @return Elemento eliminado.
	 */
	public T deQueue();
	
	/**
	 * Indica si la cola est� vac�a.
	 * @return true si est� vac�o, false de lo contrario.
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la cola.
	 * @return N�mero de elementos.
	 */
	public int size();
}
