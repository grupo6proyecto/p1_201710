package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListaEncadenada<T> implements ILista<T> {

	// -----------------------------------------------------------------
	// Clase Node
	// -----------------------------------------------------------------

	private static class Node<T> {
		private T element;
		private Node<T> next;

		public Node(T e, Node<T> n) {
			element = e;
			next = n;
		}

		public T getElement() {
			return element;
		}

		public Node<T> getNext() {
			return next;
		}

		public void setNext(Node<T> n) {
			next = n;
		}
	}

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	private Node<T> cabeza;

	private Node<T> cola;

	private int size;

	private Iterator<T> iterator;

	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	public ListaEncadenada() {
		cabeza = null;
		cola = null;
		size = 0;
		iterator = new Iterator<T>() {
			private Node<T> actual = cabeza;

			public boolean hasNext() {
				if (actual == null)
					return false;
				else if (actual.getNext() == null)
					return false;
				else
					return true;
			}

			public T next() {
				if (hasNext()) {
					actual = actual.next;
					return actual.getElement();
				} else
					throw new NoSuchElementException("No hay m�s elementos.");
			}
		};
	}

	public void addFirst(T e) {
		cabeza = new Node<>(e, cabeza);
		if (size == 0)
			cola = cabeza;
		size++;
	}

	public void add(T e) {
		Node<T> newest = new Node<>(e, null);
		if (isEmpty())
			cabeza = newest;
		else
			cola.setNext(newest);
		cola = newest;
		size++;
	}
	
	public boolean set(T e, int pos) {
		try{
			Node<T> actual = cabeza;
			for (int i = 0; i < pos; i++) {
				actual = actual.next;
			}
			actual.element = e;
			return true;
		}catch(Exception e1){
			return false;
		}
	}

	public T get(int pos) {
		if (!(pos < size) || pos < 0) {
			throw new ArrayIndexOutOfBoundsException("No existe la posici�n "+ pos);
		} else {
			Node<T> actual = cabeza;
			for (int i = 0; i < pos; i++) {
				actual = actual.next;
			}
			return actual.getElement();
		}
	}

	public int size() {
		return size;
	}

	public T remove(int pos) {
		Node <T> element;
		int size = size();
		if (cabeza == null || pos > size - 1) {
			throw new ArrayIndexOutOfBoundsException("No existe la posici�n "
					+ pos);
		} else {
			Node<T> anterior = null;
			Node<T> actual = cabeza;

			if (pos == size - 1) {
				return removeLast();
			} else if (pos == 0) {
				return removeFirst();
			} else {
				for (int i = 0; i < pos; i++) {
					anterior = actual;
					actual = actual.next;
				}
				element = actual;
				anterior.next = actual.next;
			}
		}
		return element.getElement();
	}

	public T removeFirst() {
		if (isEmpty())
			return null;
		T answer = cabeza.getElement();
		cabeza = cabeza.getNext();
		size--;
		if (size == 0)
			cola = null;
		return answer;
	}

	public T removeLast() {
		Node<T> answer = null;
		if (isEmpty())
			return null;
		else if (cabeza.next == null) 
			cabeza = null;
		else {
			Node<T> nodoTemp = cabeza;
			while (nodoTemp.getNext().getNext() != null) {
				nodoTemp = nodoTemp.next;
			}
			answer = nodoTemp.getNext();
			nodoTemp.setNext(null);
		}
		return answer.getElement();
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public Iterator<T> iterator() {
		return iterator;
	}

	public T primerElemento() {
		if (isEmpty())
			return null;
		return cabeza.getElement();
	}

	public T ultimoElemento() {
		if (isEmpty())
			return null;
		return cola.getElement();
	}
}
