package model.vo;

import model.data_structures.ILista;

public class VOGeneroUsuario implements Comparable{
	
	private String genero;
	private ILista<VOUsuarioConteo> usuarios;
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public ILista<VOUsuarioConteo> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(ILista<VOUsuarioConteo> usuarios) {
		this.usuarios = usuarios;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		if(this.compareTo(o)<0)
			return -1;
		if(this.compareTo(o)>0)
			return 1;
		return 0;
	}
	
	

}
