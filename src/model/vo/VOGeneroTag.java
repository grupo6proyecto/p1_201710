package model.vo;

import model.data_structures.ILista;

public class VOGeneroTag {

	private ILista<String> tags;
	private String genero;
	public ILista<String> getTags() {
		return tags;
	}
	public void setTags(ILista<String> tags) {
		this.tags = tags;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	
}
