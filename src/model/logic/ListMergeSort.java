package model.logic;

import java.util.Comparator;

import model.data_structures.ILista;

public class ListMergeSort {
	
	private ListMergeSort(){};
	
    /**
     * Rearranges the array in ascending order, using the natural order.
     * @param a the array to be sorted
     */
    public static void sort(ILista<Comparable> list,Comparator c) {
    	Comparable[] a = new Comparable[list.size()];
    	for (int i = 0; i < a.length; i++) {
			a[i] = list.get(i);
		}
        Comparable[] aux = new Comparable[a.length];
        sort(a, aux, 0, a.length-1,c);
    	for (int i = 0; i < a.length; i++) {
    		list.set(a[i], i);
		}
    }
    
    private static void sort(Comparable[] a, Comparable[] aux, int lo, int hi, Comparator c) {
        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        sort(a, aux, lo, mid,c);
        sort(a, aux, mid + 1, hi,c);
        merge(a, aux, lo, mid, hi,c);
    }

    private static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi,Comparator c) {
        // copy to aux[]
        for (int k = lo; k <= hi; k++) {
            aux[k] = a[k]; 
        }

        // merge back to a[]
        int i = lo, j = mid+1;
        for (int k = lo; k <= hi; k++) {
            if      (i > mid)              a[k] = aux[j++];
            else if (j > hi)               a[k] = aux[i++];
            else if (less(aux[j], aux[i],c)) a[k] = aux[j++];
            else                           a[k] = aux[i++];
        }
    }

    // is v < w ?
    private static boolean less(Comparable v, Comparable w,Comparator c) {
    	if(c == null)
    		return v.compareTo(w)<0;
        return c.compare(v, w)<0;
    }

}

	/******************************************************************************
	 *  Copyright 2002-2016, Robert Sedgewick and Kevin Wayne.
	 *
	 *  This file is part of algs4.jar, which accompanies the textbook
	 *
	 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
	 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
	 *      http://algs4.cs.princeton.edu
	 *
	 *
	 *  algs4.jar is free software: you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation, either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  algs4.jar is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  You should have received a copy of the GNU General Public License
	 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
	 ******************************************************************************/