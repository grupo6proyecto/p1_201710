package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;

import javax.swing.text.AbstractDocument.LeafElement;

import sun.nio.cs.ext.TIS_620;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;
import com.sun.xml.internal.ws.wsdl.writer.document.OpenAtts;

import model.data_structures.DoubleLinkedList;
import model.data_structures.ILista;
import model.data_structures.IStack;
import model.data_structures.ListaEncadenada;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements
		ISistemaRecomendacionPeliculas {

	/*------------------------------------------------------------------------------------------*/
	/* Atributos */
	/*------------------------------------------------------------------------------------------*/

	/**
	 * Lista de películas.
	 */
	private ILista<VOPelicula> peliculas;

	private ILista<VORating> ratings;

	private ILista<VOTag> tags;

	private ILista<VOGeneroPelicula> generos;

	private ILista<VOUsuario> usuarios;
	
	private IStack<VOOperacion> operaciones;

	private long idMayorPelicula = 0;

	/*------------------------------------------------------------------------------------------*/
	/* Constructor */
	/*------------------------------------------------------------------------------------------*/
	public SistemaRecomendacionPeliculas() {
		peliculas = new DoubleLinkedList<VOPelicula>();
		ratings = new ListaEncadenada<VORating>();
		tags = new ListaEncadenada<VOTag>();
		generos = new ListaEncadenada<VOGeneroPelicula>();
		usuarios = new DoubleLinkedList<>();
		operaciones = new Stack<>();
		cargarPeliculasSR("./data/ml-latest-small/movies.csv");
		cargarRatingsSR("./data/ml-latest-small/ratings.csv");
		cargarTagsSR("./data/ml-latest-small/tags.csv");
		cargarUsuarios("./data/ml-latest-small/ratings.csv");
	}

	/*------------------------------------------------------------------------------------------*/
	/* Lectura */
	/*------------------------------------------------------------------------------------------*/

	public boolean cargarPeliculasSR(String rutaPeliculas) {
		boolean s = false;
		try {
			FileReader reader = new FileReader(new File(rutaPeliculas));
			BufferedReader br = new BufferedReader(reader);
			String titulo = br.readLine();
			while (titulo != null) {
				titulo = br.readLine();
				long id;
				int ano;
				String[] partes = titulo.split(",");
				String genero = partes[partes.length - 1];
				id = Long.valueOf(partes[0]);

				if (partes[1].contains("" + '"'))
					titulo = partes[1].replace('"' + "", "")
							+ partes[2].replace('"' + "", "");
				else
					titulo = partes[1];
				char[] caracteres = genero.toCharArray();
				String subsub = "";
				ILista<String> gen = new DoubleLinkedList<>();
				for (int j = 0; j < caracteres.length; j++) {
					if (caracteres[j] == '|') {
						gen.add(subsub);
						subsub = "";
					} else
						subsub += caracteres[j];
				}
				gen.add(subsub);
				caracteres = titulo.toCharArray();
				ano = 0;
				if (caracteres[caracteres.length - 1] == ')') {
					String sub = "";
					int i = caracteres.length - 2;
					while (caracteres[i] != '(') {
						sub = caracteres[i] + sub;
						i--;
					}
					titulo = titulo.substring(0, i - 1);
					try {
						ano = Integer.parseInt(sub.split("-")[0]);
					} catch (NumberFormatException e) {
						titulo = titulo + " (" + sub + ")";
					}
				}
				VOPelicula pelicula = new VOPelicula();
				pelicula.setAgnoPublicacion(ano);
				pelicula.setIdPelicula(id);
				if (id > idMayorPelicula)
					idMayorPelicula = id;
				pelicula.setGenerosAsociados(gen);
				pelicula.setTitulo(titulo);
				peliculas.add(pelicula);
				boolean encontrado = false;
				for (int i = 0; i < gen.size(); i++) {
					for (int j = 0; j < generos.size() && !encontrado; j++) {
						if (gen.get(i).equals(generos.get(j).getGenero())) {
							generos.get(j).getPeliculas().add(pelicula);
							encontrado = true;
						}
					}
					if (!encontrado) {
						VOGeneroPelicula nuevoGen = new VOGeneroPelicula();
						nuevoGen.setGenero(gen.get(i));
						nuevoGen.setPeliculas(new ListaEncadenada<VOPelicula>());
						nuevoGen.getPeliculas().add(pelicula);
						generos.add(nuevoGen);
					}
				}
				s = true;
			}
			br.close();
			reader.close();
		} catch (Exception e) {

		}
		return s;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		boolean answer = false;
		try {
			FileReader reader = new FileReader(new File(rutaRatings));
			BufferedReader br = new BufferedReader(reader);
			String line = br.readLine();
			line = br.readLine();
			while (line != null) {
				String separado[] = line.split(",");
				long userId = Long.parseLong(separado[0]);
				long movieId = Long.parseLong(separado[1]);
				double rating = Double.parseDouble(separado[2]);

				VORating rat = new VORating();
				rat.setIdUsuario(userId);
				rat.setIdPelicula(movieId);
				rat.setRating(rating);
				this.ratings.add(rat);
				line = br.readLine();
			}
			answer = true;
			reader.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return answer;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		boolean answer = false;
		try {
			FileReader reader = new FileReader(new File(rutaTags));
			BufferedReader br = new BufferedReader(reader);
			String line = br.readLine();
			line = br.readLine();
			int c=1;
			while (line != null) {
				c++;
				String separado[] = line.split(",");
				String tag;
				long moivieID = Long.parseLong(separado[1]);
				long userID = Long.parseLong(separado[0]);
				long timeStamp;
				if (separado.length > 4) {
					String cadena = "";
					for (int i = 2; i < separado.length - 2; i++) {
						cadena += separado[i] + ",";
					}
					cadena += separado[separado.length - 2];
					cadena = cadena.replaceAll("\"", "");
					tag = cadena;
					timeStamp = Long.parseLong(separado[separado.length - 1]);
				} else {
					tag = separado[2];
					timeStamp = Long.parseLong(separado[3]);
				}
				VOTag tags = new VOTag();
				tags.setTag(tag);
				tags.setTimestamp(timeStamp);
				this.tags.add(tags);
				line = br.readLine();
				// agregarTagPelicula(tags, moivieID);
				// agregarTagUsuario(tags, userID);
			}
			answer = true;
			reader.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return answer;
	}

	/*------------------------------------------------------------------------------------------*/
	/* Métodos */
	/*------------------------------------------------------------------------------------------*/

	@Override
	public int sizeMoviesSR() {
		return peliculas.size();
	}

	@Override
	public int sizeUsersSR() {
		return usuarios.size();
	}

	@Override
	public int sizeTagsSR() {
		return tags.size();
	}

	/**
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 * --------------------------------------------------------------- A
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 */

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		ILista<VOGeneroPelicula> lista = new DoubleLinkedList<>();
		ILista<Comparable> comparable = new DoubleLinkedList<>();
		ILista<VOPelicula> temp = new DoubleLinkedList<>();
		VOGeneroPelicula genero;
		for (int i = 0; i < ratings.size(); i++) {
			comparable.add(ratings.get(i));
		}
		ListMergeSort.sort(comparable, null);
		for (int i = 0; i < comparable.size(); i++) {
			temp.add((VOPelicula) comparable.get(i));
		}
		for (VOGeneroPelicula generoPelicula : generos) {
			temp = generoPelicula.getPeliculas();
			genero = new VOGeneroPelicula();
			genero.setGenero(generoPelicula.getGenero());
			genero.setPeliculas(new DoubleLinkedList<VOPelicula>());

			for (int i = temp.size() - 1; i > temp.size() - n - 1 && i > 0; i--) {
				genero.getPeliculas().add(temp.get(i));
			}
			lista.add(genero);
		}
		return lista;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		cargarPeliculasSR("./data/ml-latest-small/movies.csv");
		ILista<VOPelicula> lista = new DoubleLinkedList<>();
		ILista<Comparable> comparable = new DoubleLinkedList<>();
		for (int i = 0; i < peliculas.size(); i++) {
			if (peliculas.get(i).getAgnoPublicacion() != peliculas.get(i + 1)
					.getAgnoPublicacion())
				comparable.add(peliculas.get(i).getAgnoPublicacion());
			else {
				if (peliculas.get(i).getPromedioRatings() != peliculas.get(
						i + 1).getPromedioRatings())
					comparable.add(peliculas.get(i).getPromedioRatings());
				else
					comparable.add(peliculas.get(i).getTitulo());
			}
		}
		ListMergeSort.sort(comparable, null);
		for (int i = 0; i < comparable.size(); i++) {
			lista.add((VOPelicula) comparable.get(i));
		}
		return lista;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		cargarPeliculasSR("./data/ml-latest-small/movies.csv");
		ILista<VOGeneroPelicula> lista = new DoubleLinkedList<>();
		ILista<Comparable> comparable = new DoubleLinkedList<>();
		ILista<VOPelicula> temp;
		VOGeneroPelicula genero;
		for (int i = 0; i < peliculas.size(); i++) {
			comparable.add(peliculas.get(i).getPromedioRatings());
		}
		for (VOGeneroPelicula generoPelicula : generos) {
			temp = generoPelicula.getPeliculas();
			ListMergeSort.sort(comparable, null);

			genero = new VOGeneroPelicula();
			genero.setGenero(generoPelicula.getGenero());
			genero.setPeliculas(new DoubleLinkedList<VOPelicula>());

			for (int i = temp.size() - 1; i > 0; i--) {
				genero.getPeliculas().add(temp.get(i));
			}
			lista.add(genero);
		}
		return lista;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		/**
		 * El SR debe retornar una lista con las películas y los tags asociados
		 * a cada película, de un género determinado, ordenado por fecha de
		 * publicación de la película. Cada posición de la lista tiene un género
		 * y una lista por cada película perteneciente a dicho género. Cada
		 * película debe contener sus tags asociados.
		 */
		
		return null;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(
			String rutaRecomendacion, int n) {
		/**
		 * El SR debe retornar una lista de películas que pueden interesarle a
		 * un usuario. Para ello analiza las últimas n películas vistas por
		 * dicho usuario. Las películas vistas recientemente por un usuario
		 * deben ser leídas de un archivo recomendacion.csv. Este archivo
		 * contiene en cada línea un id de película y una calificación del
		 * rating, separado por una coma. La información registrada en este
		 * archivo debe ser leída y debe ser almacenada en una cola para luego
		 * ser procesada. El SR, procesa cada película y para cada una de ellas
		 * busca películas que tengan dentro de sus géneros el género principal
		 * registrado de la película analizada (primer género en el archivo) con
		 * un rating promedio similar (no difiere en mas .5). El SR retorna una
		 * lista, en donde en cada posición está la película analizada y una
		 * lista con las películas recomendadas. El listado de películas
		 * recomendadas debe estar ordenado de menor a mayor diferencia absoluta
		 * entre el rating asignado por el usuario y el rating promedio. El
		 * archivo es proporcionado por el usuario del SR, en esa medida para
		 * efectos de prueba, usted debe crear un archivo recomendacion.csv que
		 * sea consistente con los datos en el catálogo de películas.
		 */
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		/**
		 * El SR debe retornar una lista con las calificaciones de una película
		 * determinada, ordenadas por la fecha de creación del rating (de la mas
		 * reciente a la menos reciente).
		 */
		return null;
	}

	/**
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 * --------------------------------------------------------------- B
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 */
	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		ILista<VOGeneroUsuario> rta = new DoubleLinkedList<>();

		/* Se empieza por cada genero */
		for (int i = 0; i < generos.size(); i++) {
			/* VOGeneroUsuario actual */
			VOGeneroUsuario actual = new VOGeneroUsuario();
			actual.setGenero(generos.get(i).getGenero());
			ILista<Comparable> r = new DoubleLinkedList<>();
			/*---------------//--------------*/

			/* VOUsuarioConteoActual para añadir */
			VOUsuarioConteo aux = new VOUsuarioConteo();
			aux.setIdUsuario(ratings.get(0).getIdUsuario());
			aux.setConteo(0);

			/* Busqueda de peliculas de dicho genero */
			for (int j = 0; j < ratings.size(); j++) {

				/* Id del usuario actual */
				long a = ratings.get(j).getIdUsuario();

				/* Es el usuario el mismo? */
				if (aux.getIdUsuario() == a) {
					/* Es la pelicula del genero actual? */
					if (existeGenero(darPeliculaId(ratings.get(j).getIdPelicula()),actual.getGenero())) {
						aux.setConteo(aux.getConteo() + 1);
					}
				}
				/* Que ocurre si es otro usuario? */
				if (ratings.get(j + 1).getIdUsuario() != aux.getIdUsuario()) {
					/* Se añade el VOUsuarioConteo (de dicho genero) */
					r.add(aux);
					/* Se reinicia el VOGeneroContro */
					aux.setIdUsuario(ratings.get(j + 1).getIdUsuario());
					aux.setConteo(0);
				}
			}
			/* Se ordenan los usuarios por su actividad */
			ListMergeSort.sort(r, new UserActivityComparator());
			/* Lista especifica de VOUsuarios */
			ILista<VOUsuarioConteo> s = new DoubleLinkedList<>();
			/* Creacion de lista de los n usuarios mas activos */
			for (int j = 0; j < n; j++) {
				s.add((VOUsuarioConteo) r.get(j));
			}
			actual.setUsuarios(s);
			rta.add(actual);
		}
		return rta;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		ILista<VOUsuario> usua = new DoubleLinkedList<>();
		ILista<Comparable> usr = new DoubleLinkedList<>();
		for (int i = 0; i < usuarios.size(); i++) {
			usr.add(usuarios.get(i));
		}
		ListMergeSort.sort(usr, null);
		for (int i = 0; i < usr.size(); i++) {
			usua.add((VOUsuario) usr.get(i));
		}
		return usuarios;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		ILista<VOGeneroPelicula> rta = new DoubleLinkedList<>();

		/* Se empieza por cada genero */
		for (int i = 0; i < generos.size(); i++) {
			/* VOGeneroUsuario actual */
			VOGeneroPelicula actual = new VOGeneroPelicula();
			actual.setGenero(generos.get(i).getGenero());
			ILista<Comparable> r = new DoubleLinkedList<>();
			/*---------------//--------------*/

			/* Busqueda de peliculas de dicho genero */
			for (int j = 0; j < peliculas.size(); j++) {
				if (existeGenero(peliculas.get(j),actual.getGenero()))
					r.add(peliculas.get(j));
			}
			/* Se ordenan los usuarios por su actividad */
			ListMergeSort.sort(r, new TagsMovieComparator());
			/* Lista especifica de VOUsuarios */
			ILista<VOPelicula> s = new DoubleLinkedList<>();
			/* Creacion de lista de los n usuarios mas activos */
			for (int j = 0; j < n; j++) {
				s.add((VOPelicula) r.get(j));
			}
			actual.setPeliculas(s);
			;
			rta.add(actual);
		}
		return rta;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		ILista<VOUsuarioGenero> rta = new DoubleLinkedList<>(); 
		ILista<VOUsuario> usr = new DoubleLinkedList<>();
		for (int i = 0; i < usr.size(); i++) {
			VOUsuarioGenero actual = new VOUsuarioGenero();
			VOUsuario a = usr.get(i);
			actual.setIdUsuario(a.getIdUsuario());
			for (int j = 0; j < generos.size(); j++) {
				String gen = generos.get(j).getGenero();
				ILista<VOGeneroTag> generoTags = new DoubleLinkedList<>();
				VOGeneroTag asd = new VOGeneroTag();
				asd.setGenero(gen);
				for (int k = 0; k < peliculas.size(); k++) {
					VOPelicula n = peliculas.get(k);
					if(existeGenero(n,gen)){
					ILista<String> asb = new DoubleLinkedList<>();
						for (int l = 0; l <a.getTagsAsociados().size(); l++) {
							String b = a.getTagsAsociados().get(l);
							if(existeTag(n,b)){
								asb.add(b);
							}	
						}
						asd.setTags(asb);
					}
				}
				
			}
			rta.add(actual);
		}
		return rta;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		for (int i = 0; i < peliculas.size(); i++) {
			if(peliculas.get(i).getIdPelicula()==idPelicula)
				return peliculas.get(i).getTagsAsociados();
		}
		return null;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		VOOperacion op = new VOOperacion();
		op.setOperacion(nomOperacion);
		op.setTimestampInicio(tinicio);
		op.setTimestampFin(tfin);
		operaciones.push(op);
	}

	/**
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 * --------------------------------------------------------------- C
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 * ---------------------------------------------------------------
	 */
	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		ILista<VOOperacion> lista = new DoubleLinkedList<>();
		for (int i = 0; i < operaciones.size(); i++) {
			lista.add(operaciones.pop());
		}
		for (int i = 0; i<operaciones.size(); i++) {
			operaciones.push(lista.get(i));
		}
		return lista;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub
		for (int i = 0; i < operaciones.size(); i++) {
			operaciones.pop();
		}
	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		ILista<VOOperacion> rta = new DoubleLinkedList<>();
		for (int i = 0; i < n; i++) {
			rta.add(operaciones.pop());
		}
		for(int i = n; i > 0; i--) {
			operaciones.push(rta.get(i));
		}
		return rta;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		for (int i = 0; i < n; i++) {
			operaciones.pop();
		}
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		VOPelicula fil = new VOPelicula();
		fil.setTitulo(titulo);
		fil.setAgnoPublicacion(agno);
		ILista<String> act = new DoubleLinkedList<String>();
		for (int i = 0; i < generos.length; i++) {
			act.add(generos[i]);
		}
		fil.setGenerosAsociados(act);
		fil.setIdPelicula(++idMayorPelicula);
		fil.setNumeroRatings(0);
		fil.setNumeroTags(0);
		fil.setTagsAsociados(null);
		return idMayorPelicula;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		VORating finalista = new VORating();
		finalista.setIdUsuario(idUsuario);
		finalista.setIdPelicula(idPelicula);
		finalista.setRating(rating);
		ratings.add(finalista);
	}

	// -----------------------------------------------------
	// Metodos auxiliares:
	// -----------------------------------------------------

	/**
	 * Metodo que regresa la opelicula dada su ID
	 * 
	 * @param l
	 * @return
	 */
	private VOPelicula darPeliculaId(long l) {
		for (int i = 0; i < peliculas.size(); i++) {
			if (peliculas.get(i).getIdPelicula() == l) {
				return peliculas.get(i);
			}
		}
		return null;
	}

	/**
	 * Carga en una lista los uauaruios del archivo Ratings
	 * 
	 * @return
	 */
	private void cargarUsuarios(String rutaRatings) {
		ILista<VOUsuario> rta = new DoubleLinkedList<>();
		try {
			FileReader reader = new FileReader(new File(rutaRatings));
			BufferedReader br = new BufferedReader(reader);
			String line = br.readLine();
			line = br.readLine();
			/* Usuario nuevo */
			VOUsuario actual = new VOUsuario();
			actual.setIdUsuario(-1);
			actual.setPrimerTimestamp(Long.MAX_VALUE);
			int contadorRatings = 0;
			/* Reinicializacion */
			while (line != null) {
				String separado[] = line.split(",");
				long userId = Long.parseLong(separado[0]);
				long movieId = Long.parseLong(separado[1]);
				double rating = Double.parseDouble(separado[2]);
				long first = Long.parseLong(separado[3]);

				if (actual.getIdUsuario() == -1)
					actual.setIdUsuario(userId);
				if (actual.getIdUsuario() == userId) {
					contadorRatings++;
					if (first < actual.getPrimerTimestamp())
						actual.setPrimerTimestamp(first);
				} else {
					rta.add(actual);
					actual = new VOUsuario();
					actual.setIdUsuario(-1);
					actual.setPrimerTimestamp(Long.MAX_VALUE);
					contadorRatings = 0;
					/* Reinicializacion */
				}
			}
			reader.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		usuarios = rta;
	}

	/**
	 * Inserta un tag en una pelicula especifica.
	 * 
	 * @param tag
	 * @param peliculaID
	 */
	private void agregarTagPelicula(VOTag tag, long peliculaID) {
		boolean find = false;
		for (int i = 0; i < peliculas.size()&&!find; i++) {
			VOPelicula a = peliculas.get(i);
			if (a.getIdPelicula() == peliculaID) {
				ILista<VOTag> as = null;
				if (a.getTagsAsociados() == null)
					as = new DoubleLinkedList<VOTag>();
				as.add(tag);
				a.setTagsAsociados(as);
				peliculas.set(a, i);
				find = true;
			}
		}
	}

	private void agregarTagUsuario(VOTag tag, long userID) {
		boolean find = false;
		for (int i = 0; i < usuarios.size()&&!find; i++) {
			VOUsuario a = usuarios.get(i);
			if (a.getIdUsuario() == userID) {
				ILista<String> as = null;
				if (a.getTagsAsociados() == null)
					as = new DoubleLinkedList<String>();
				as.add(tag.getTag());
				a.setTagsAsociados(as);
				usuarios.set(a, i);
				find = true;
			}
		}
	}

	
	//Metodos auxialiares de VOPelicula
	
	/**
	 * Dice si la pelicula actual tiene un genero g.
	 * @param genero
	 * @return
	 */	
	private boolean existeGenero(VOPelicula a,String genero){
		for (int i = 0; i < a.getGenerosAsociados().size() ; i++) {
			if(a.getGenerosAsociados().get(i)==genero)
				return true;
		}
		return false;
	}
	
	/**
	 * Dice si la pelicula tiene un tag.
	 * @param genero
	 * @return
	 */	
	private boolean existeTag(VOPelicula a,String tag){
		for (int i = 0; i < a.getTagsAsociados().size() ; i++) {
			if( a.getTagsAsociados().get(i).equals(tag))
				return true;
		}
		return false;
	}
}
