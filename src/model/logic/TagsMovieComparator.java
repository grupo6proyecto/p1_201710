/**
 * 
 */
package model.logic;

import java.util.Comparator;

import model.vo.VOPelicula;

/**
 * @author Mat
 *
 */
public class TagsMovieComparator implements Comparator<VOPelicula>{

	public int compare(VOPelicula arg0, VOPelicula arg1) {
		// TODO Auto-generated method stub
		return arg1.getNumeroTags()-arg0.getNumeroTags();
	}

}
