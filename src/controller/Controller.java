package controller;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class Controller {
	
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------
	
	private ISistemaRecomendacionPeliculas model;
	
	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	
	public Controller(){
		model = new SistemaRecomendacionPeliculas();
	}
	
	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------
	
	public int sizeMovies(){
		return model.sizeMoviesSR();
	}
	public int sizeTags(){
		return model.sizeTagsSR();
	}
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n){
		return model.peliculasPopularesSR(n);
	}
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR(){
		return model.catalogoPeliculasOrdenadoSR();
	}
	public ILista<VOGeneroPelicula> recomendarGeneroSR(){
		return model.recomendarGeneroSR();
	}
}
