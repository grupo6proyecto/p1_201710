package model.data_structures_test;

import model.data_structures.IQueue;
import model.data_structures.Queue;
import junit.framework.TestCase;

public class QueueTest extends TestCase {

	// -----------------------------------------------------------------
	// Lista
	// -----------------------------------------------------------------

	/**
	 * Cola a probar.
	 */
	private IQueue<String> cola;

	// -----------------------------------------------------------------
	// Escenarios
	// -----------------------------------------------------------------

	/**
	 * Escenario vac�o.
	 */
	public void setupEscenario1() {

		/* Se inicializa la lista */
		cola = new Queue<String>();
	}

	/**
	 * Escenario con elementos.
	 */
	public void setupEscenario2() {

		/* Se inicializa la lista */
		cola = new Queue<String>();
		for (int i = 1; i <= 1000; i++) {
			cola.enQueue("Elemento " + i);
		}
	}

	// -----------------------------------------------------------------
	// Pruebas
	// -----------------------------------------------------------------

	/**
	 * Prueba el m�todo enQueue de la cola.
	 */
	public void testEnQueue() {

		/*----------------------------------------------------------------*/
		/* Prueba con un escenario vac�o */
		/*----------------------------------------------------------------*/
		setupEscenario1();
		cola.enQueue("Queens");

		assertEquals("Deber�a ser Queens", "Queens", cola.deQueue());

		/*----------------------------------------------------------------*/
		/* Prueba con un escenario con elementos */
		/*----------------------------------------------------------------*/

		setupEscenario2();
		cola.enQueue("Queens");
		for (int i = 0; i < 1000; i++)
			cola.deQueue();
		assertEquals("Deber�a ser Queens", "Queens", cola.deQueue());
	}

	/**
	 * Prueba el m�todo deQueue de la cola.
	 */
	public void testDeQueue() {

		/*----------------------------------------------------------------*/
		/* Prueba con un escenario vac�o */
		/*----------------------------------------------------------------*/
		setupEscenario1();
		boolean x = false;
		try{
			cola.deQueue();
		}catch(Exception e){
			x = true;
		}
		assertTrue("Deberia lanzar excepcion.",x);

		/*----------------------------------------------------------------*/
		/* Prueba con un escenario con elementos */
		/*----------------------------------------------------------------*/

		setupEscenario2();
		for (int i = 1; i <= 1000; i++)
			assertEquals("Deber�a ser 'Elemento i'", "Elemento " + i,cola.deQueue());
		
		
	}

	/**
	 * Prueba el m�todo isEmpty de la cola.
	 */
	 public void testIsEmpty(){
	
	 /*----------------------------------------------------------------*/
	 /*Prueba con un escenario vac�o*/
	 /*----------------------------------------------------------------*/
	 setupEscenario1();
	 assertTrue("Deber�a ser true pues la cola est� vac�a", cola.isEmpty());
	
	 /*----------------------------------------------------------------*/
	 /*Prueba con un escenario con elementos*/
	 /*----------------------------------------------------------------*/
	 setupEscenario2();
	 assertFalse("Deber�a ser false pues la cola tiene elementos", cola.isEmpty());
	 }
	
	 /**
	  * Prueba el m�todo size de la cola.
	  */
	 public void testSize(){
	
	 /*----------------------------------------------------------------*/
	 /*Prueba con un escenario vac�o*/
	 /*----------------------------------------------------------------*/
	 setupEscenario1();
	 assertEquals("Deber�a ser cero", 0, cola.size());
	
	 /*----------------------------------------------------------------*/
	 /*Prueba con un escenario con elementos*/
	 /*----------------------------------------------------------------*/
	 setupEscenario2();
	 	assertEquals("Deberian ser mil",1000,cola.size());
	 }
}
