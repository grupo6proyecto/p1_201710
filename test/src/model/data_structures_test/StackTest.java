package model.data_structures_test;

import model.data_structures.IStack;
import model.data_structures.Stack;
import junit.framework.TestCase;

public class StackTest extends TestCase{
	
	// -----------------------------------------------------------------
	// Lista
	// -----------------------------------------------------------------
	
	/**
	 * Pila a probar
	 */
	private IStack<String> pila;
	
	
	// -----------------------------------------------------------------
	// Escenarios
	// -----------------------------------------------------------------
	
	/**
	 * Escenario vac�o.
	 */
	public void setupEscenario1(){
		
		/*	Se inicializa la lista  */
		pila = new Stack<String>();
	}
	
	
	/**
	 * Escenario con elementos
	 */
	public void setupEscenario2(){

		/*	Se inicializa la lista  */
		pila = new Stack<String>();
		for (int i=1; i <= 1000; i++) {
			pila.push("Elemento " + i);
		}
	}
	
	// -----------------------------------------------------------------
	// Pruebas
	// -----------------------------------------------------------------
	
	/**
	 * Prueba el m�todo push en la pila.
	 */
	public void testPush(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		pila.push("Queens");
		assertEquals("Deber�a ser 'Queens'","Queens",pila.pop());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		
		setupEscenario2();
		pila.push("Queens");
		assertEquals("Deber�a ser 'Queens'","Queens",pila.pop());
	}
	
	/**
	 * Prueba el m�todo pop en la pila.
	 */
	public void testPop(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		assertNull("Deber�a ser null pues la pila est� vac�a",pila.pop());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/		
		setupEscenario2();
		for (int i=pila.size(); i>0;i--){
			assertEquals("Deber�a ser 'Elemento seg�n el indice'","Elemento "+i, pila.pop());
		}
	}
	
	/**
	 * Prueba el m�todo isEmpty de la pila.
	 */
	public void testIsEmpty(){
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		assertTrue("Deber�a ser true pues la pila est� vac�a",pila.isEmpty());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		setupEscenario2();
		assertFalse("Deber�a ser false pues la pila tiene elementos",pila.isEmpty());
	}

	/**
	 * Prueba el m�todo size de la pila.
	 */
	public void testSize(){
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		assertEquals("Deberia ser cero",0,pila.size());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		setupEscenario2();
		assertEquals("Deberian ser mil",1000,pila.size());
	}
}
